#include <cmath>
#include <iostream>

using namespace std;

int main() {
    double a, b, c;

    cout << "Please enter a value for a, b and c! (ax^2 + bx + c = 0)" << endl;
    cout << "a: ";
    cin >> a;
    cout << "b: ";
    cin >> b;
    cout << "c: ";
    cin >> c;
    cout << "---" << endl;
    cout << "Equation: " << a << "x^2 + " << b << "x + " << c << " = 0" << endl;

    // catch corner case
    if (a == 0) {
        // return nan, casue equation is not quadratic
        cerr << "The equation is not quadratic (a is 0)!";
        return -1;
    }

    // convert into normal form
    double p = b / a;
    double q = c / a;

    // calculate discriminant and solutions
    double discriminant = pow(p / 2, 2) - q;

    if (discriminant > 0) {
        // equation has two real solutions
        cout << "x1: " << -(p / 2) - sqrt(discriminant)
             << "\tx2: " << -(p / 2) + sqrt(discriminant) << endl;
    } else if (discriminant == 0) {
        // euqation has exacly one real solution
        cout << "x1 / x2: " << -(p / 2) << endl;
    } else if (discriminant < 0)
        // euqation has two imaginary solutions
        cout << "x1 / x2: "
             << "imaginary" << endl;

    return 0;
}