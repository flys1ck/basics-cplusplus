#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
    int attempts = 0;
    int guessed = -1;
    int max;
    // read maximum
    cout << "Please enter a maximum value for [0, max] to guess within: ";
    cin >> max;

    // calc random number
    int secret = rand() % (max + 1);

    // let user search
    while (secret != guessed) {
        attempts++;

        cout << "Guess the secret (0.." << max << "): ";
        cin >> guessed;

        if (secret < guessed)
            cout << "The secret number is lower" << endl;
        else if (secret > guessed)
            cout << "The secret number is higher" << endl;
    }

    cout << "Congratulations, you found " << secret << " in " << attempts
         << " attempts!" << endl;

    return 0;
}