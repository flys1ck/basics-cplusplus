#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

// returns greatest value in vector
double max(const vector<double> &v) {
    double max = v[0];
    for (double d : v) {
        if (d > max) {
            max = d;
        }
    }

    return max;
};

// returns true, if all values are positive, else false
bool allPositive(const vector<double> &v) {
    bool isPositive = true;
    for (double d : v) {
        if (d < 0) {
            isPositive = false;
            break;
        }
    }

    return isPositive;
};

// returns scalar product of v1 and v2
double product(const vector<double> &v1, const vector<double> &v2) {
    double product = 0;
    for (int i = 0; i < v1.size(); ++i) {
        product += v1[i] * v2[i];
    }

    return product;
};

// returns product of vector with f
vector<double> product(const vector<double> &v, double f) {
    vector<double> prod(v.size());
    for (int i = 0; i < v.size(); ++i) {
        prod[i] += v[i] * f;
    }

    return prod;
};

// returns mean of vector
double norm(const vector<double> &v) {
    double sum = 0;
    for (double d : v) {
        sum += pow(d, 2);
    }

    return sqrt(sum);
};

// normalizes vector (independent of functions used before)
void normalize(vector<double> &v) {
    double norm;
    double sum = 0;
    for (double d : v) {
        sum += pow(d, 2);
    }

    norm = sqrt(sum);

    for (double &d : v) {
        d = d / norm;
    }

    return;
};

// helper function to print vector values
void printVector(const vector<double> &v) {
    cout << "( ";
    for (double d : v) {
        cout << d << " ";
    }
    cout << ")" << endl;

    return;
}

// helper function to read in user input
void readVectorFromCin(vector<double> &v, int size) {
    double input;
    cout << "Enter " << size << " coefficients (double) seperated by space: ";
    while (v.size() < size && cin >> input) {
        v.push_back(input);
    }

    return;
}

int main() {
    // user input for size of the vector, since vectors need same dimension for
    // scalar product
    int size;
    cout << "Define size (integer, >0) of vectors: ";
    cin >> size;
    if (size < 1) {
        cerr << "ERROR: size is lesser than 1. Restart to try again!";
        return -1;
    }

    // user input for vector a and b
    vector<double> a;
    vector<double> b;
    cout << "Define vector a: ";
    readVectorFromCin(a, size);
    cout << "Define vector b: ";
    readVectorFromCin(b, size);

    // user input for factor
    double factor;
    cout << "Define factor (double): ";
    cin >> factor;
    cout << "---" << endl;

    // print vectors
    cout << "Vector a: ";
    printVector(a);
    cout << "Vector b: ";
    printVector(b);
    cout << "---" << endl;

    // make calculations
    cout << "Maximum of a: " << max(a) << endl;
    cout << "Maximum of b: " << max(b) << endl;

    cout << "Is a completely positive?: " << (allPositive(a) ? "yes" : "no")
         << endl;
    cout << "Is b completely positive?: " << (allPositive(b) ? "yes" : "no")
         << endl;

    cout << "Scalar Product of a and b: " << product(a, b) << endl;

    cout << "Product of a with " << factor << ": ";
    vector<double> prod = product(a, factor);
    printVector(prod);
    cout << "Product of b with " << factor << ": ";
    prod = product(b, factor);
    printVector(prod);

    cout << "Norm of a: " << norm(a) << endl;
    cout << "Norm of b: " << norm(b) << endl;

    normalize(a);
    cout << "Normalized a: ";
    printVector(a);

    normalize(b);
    cout << "Normalized b: ";
    printVector(b);

    return 0;
}