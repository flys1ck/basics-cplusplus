#include <cassert>
#include <iostream>
#include <string>

using namespace std;

int convert(const string &s) {
    int n = 0;
    bool hasFoundDigits = false; // true, if a digit has been found in string

    for (int i = 0; i < s.length(); ++i) {
        //  check position of space
        if (s[i] == ' ') {
            // if hasFoundDigits is true, the space is behind number
            if (hasFoundDigits) {
                break;
            }
            continue;
        }

        // convert number to int
        if (s[i] >= '0' && s[i] <= '9') {
            n = n * 10 + s[i] - '0';
            hasFoundDigits = true;
        } else {
            cerr << "ERROR: string contains invalid characters" << endl;
            return -1;
        }
    }

    return n;
};

int main() {
    // user input
    string input;
    cout << "Enter number (integer), which should be converted: ";
    getline(cin, input);
    cout << convert(input) << endl;

    return 0;
}
