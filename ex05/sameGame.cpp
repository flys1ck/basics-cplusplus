#include <algorithm>
#include <iostream>
#include <random>
#include <string>
#include <vector>

using namespace std;

/** Represents a field on a 2D board, defined by it's row and column. */
struct Field {
    int row;    // Row on the board
    int column; // Column on the board
};

/**
 * Initializes the given game board with random numbers between 2 and 6. Each
 * number corresponds to a different color for a field on the board. This
 * function modifies the game board.
 *
 * @param board vector of vectors with integer values
 */
void initBoard(vector<vector<int>> &board) {
    // mt_19937
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(0, 4);

    // generating BOARD_SIZE*BOARD_SIZE numbers
    for (int row = 0; row < board[0].size(); ++row) {
        for (int column = 0; column < board.size(); ++column) {
            board[row][column] = dis(gen);
        }
    }

    return;
}

/**
 * Prints the board to the command line using ANSI codes. The integer values in
 * the given game board will be interpreted as defined below:
 *
 * 0 - red field
 * 1 - green field
 * 2 - yellow field
 * 3 - blue field
 * 4 - magenta field
 * 5 - field marked for deletion (will never get printed)
 * 6 - empty field
 *
 * @param board vector of vectors with integer values
 */
void printBoard(vector<vector<int>> &board) {
    for (int row = 0; row < board[0].size(); ++row) {
        for (int column = 0; column < board.size(); ++column) {
            switch (board[row][column]) {
            case 0:
                cout << "\033[0;41m   \033[0m";
                break;
            case 1:
                cout << "\033[0;42m   \033[0m";
                break;
            case 2:
                cout << "\033[0;43m   \033[0m";
                break;
            case 3:
                cout << "\033[0;44m   \033[0m";
                break;
            case 4:
                cout << "\033[0;45m   \033[0m";
                break;
            case 6:
                cout << "   ";
                break;
            default:
                cout << board[row][column] << " ";
                cerr << "ERROR: This should never happen!\n";
                return;
            }
        }
        cout << " " << row + 1 << endl;
    }
    for (int i = 0; i < board.size(); ++i) {
        cout << " " << static_cast<char>('a' + i) << " ";
    }
    cout << "\n";
}

/**
 * Asks the player for input, validates and translates it into row - column
 * coordinates.
 *
 * @returns Field, which includes the player input
 */
Field getPlayerInput(vector<vector<int>> &board) {
    string input;
    cout << "Enter a coordinates of a field with adjacent colors (character "
            "first, f.e. 'a1'): ";
    cin >> input;

    // validate input

    Field move;
    move.row = input[1] - '1';
    move.column = input[0] - 'a';

    return move;
}

/**
 * Asks the player for input, validates and translates it into row - column
 * coordinates.
 *
 * @returns vector of Field, containing group of adjacent colors
 */
vector<Field> getSameGroup(vector<vector<int>> &board, Field &move) {
    // check 4 directions
    vector<Field> queue;
    vector<Field> points;
    Field currentPoint;
    Field newP;
    bool visited;
    queue.push_back(move);

    while (queue.size() > 0) {
        currentPoint = queue.back();
        queue.pop_back();
        visited = false;

        for (Field p : points) {
            if (p.row == currentPoint.row && p.column == currentPoint.column) {
                visited = true;
                break;
            }
        }

        if (visited) {
            continue;
        }

        points.push_back(currentPoint);

        // left
        if (currentPoint.row - 1 >= 0 &&
            board[currentPoint.row][currentPoint.column] ==
                board[currentPoint.row - 1][currentPoint.column]) {
            newP.row = currentPoint.row - 1;
            newP.column = currentPoint.column;
            queue.push_back(newP);
        }

        // right
        if (currentPoint.row + 1 < board.size() &&
            board[currentPoint.row][currentPoint.column] ==
                board[currentPoint.row + 1][currentPoint.column]) {
            newP.row = currentPoint.row + 1;
            newP.column = currentPoint.column;
            queue.push_back(newP);
        }
        // top
        if (currentPoint.column + 1 < board[0].size() &&
            board[currentPoint.row][currentPoint.column] ==
                board[currentPoint.row][currentPoint.column + 1]) {
            newP.row = currentPoint.row;
            newP.column = currentPoint.column + 1;
            queue.push_back(newP);
        }
        // bottom
        if (currentPoint.column - 1 >= 0 &&
            board[currentPoint.row][currentPoint.column] ==
                board[currentPoint.row][currentPoint.column - 1]) {
            newP.row = currentPoint.row;
            newP.column = currentPoint.column - 1;
            queue.push_back(newP);
        }
    }

    return points;
}

/**
 * Assigns the temporary value of 5 for fields, which will be deleted.
 *
 * @returns vector of Field, containing group of adjacent colors
 */
void deleteGroup(vector<vector<int>> &board, vector<Field> &v) {
    if (v.size() > 1) {
        for (Field p : v) {
            board[p.row][p.column] = 5;
        }
    } else {
        cout << "Please select groups with more than 1 stone.\n";
    }

    return;
}

/**
 * Collapes rows with empty fields. This function modifies the game board.
 *
 * @returns vector of Field, containing group of adjacent colors
 */
void collapseRows(vector<vector<int>> &board) {
    int currentRow;
    for (int row = 0; row < board.size(); ++row) {
        for (int column = 0; column < board[0].size(); ++column) {
            // check if field is marked for deletion
            if (board[row][column] == 5) {
                currentRow = row;
                while (currentRow > 0) {
                    // copy each element down
                    board[currentRow][column] = board[currentRow - 1][column];
                    --currentRow;
                }
                board[0][column] = 6;
            }
        }
    }

    return;
}

/**
 * Pushes columns together. This function modifies the game board.
 *
 * @returns vector of Field, containing group of adjacent colors
 */
void pushColumns(vector<vector<int>> &board) {
    vector<int> deleteQueue;

    for (int column = board[0].size() - 1; column >= 0; --column) {
        // empty columns have empty fields in the last row
        if (board[board.size() - 1][column] == 6) {
            cout << column << "\n";
            deleteQueue.push_back(column);
        }
    }

    for (int col : deleteQueue) {
        for (int row = 0; row < board.size(); ++row) {
            board[row].erase(board[row].begin() + col - 1);
        }
    }
};

bool isMovePossible(vector<vector<int>> &board) {
    for (int row = 0; row < board.size(); ++row) {
        for (int column = 0; column < board[0].size(); ++column) {
            // Only check right and bottom neighbour, since the iteration goes
            // from left to right and top to bottom. Skip empty fields. Respect
            // board limits.
            if (board[row][column] != 6 &&
                (row + 1 < board.size() || column + 1 < board[0].size()) &&
                (board[row][column] == board[row + 1][column] ||
                 board[row][column] == board[row][column + 1])) {
                return true;
            }
        }
    }

    return false;
}

int main(int argc, char const *argv[]) {
    const int BOARD_ROWS = 9;
    const int BOARD_COLUMNS = 9;

    vector<vector<int>> board(BOARD_COLUMNS, vector<int>(BOARD_ROWS));
    vector<Field> currentGroup;
    Field move;
    int points = 0;
    cout << endl;

    initBoard(board);
    printBoard(board);

    // moves possible?
    while (isMovePossible(board)) {
        // calulate points
        cout << "Current points: " << points << "\n";
        points += currentGroup.size() * (currentGroup.size() - 1);
        move = getPlayerInput(board);
        currentGroup = getSameGroup(board, move);

        // shrink area
        deleteGroup(board, currentGroup);
        collapseRows(board);
        // pushColumns(board);
        cout << endl;
        printBoard(board);
        cout << "----------\n";
    }

    cout << "No more moves possible. Game has ended.\n";
    cout << "Total points: " << points << "\n";

    return 0;
}
