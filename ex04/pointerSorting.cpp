#include <iostream>
#include <string>
#include <vector>

using namespace std;

// Returns a vector of pointers referencing string values of the given vector
vector<string *> getPointerVector(vector<string> &strings) {
    vector<string *> pStrings;

    for (int i = 0; i < strings.size(); ++i) {
        pStrings.push_back(&strings[i]);
    }

    return pStrings;
}

/**
 * @brief Sorts string values, which are referenced by pointers in a given
 * vector, with selection sort O(n^2).
 *
 * @param pStrings vector containing pointers to string values
 * @return vector of pointers, which is sorted by values the pointers are
 * referencing to
 */
void sort(vector<string *> &pStrings) {
    int minIdx; // contains index of the lowest element

    // first loop: determines size of vector / subvector
    // second loop: checks, if smaller elements exist in vector /subvector
    for (int i = 0; i < pStrings.size() - 1; ++i) {
        minIdx = i;
        for (int j = i + 1; j < pStrings.size(); ++j) {
            if (*pStrings[j] < *pStrings[minIdx]) {
                minIdx = j;
            }
        }
        // put lowest element on first position in vector / subvector
        swap(pStrings[minIdx], pStrings[i]);
    }

    return;
}

// Prints string values, which are refereced by pointers in a given vector
void printVector(const vector<string *> &pStrings) {
    cout << "( ";
    for (string *s : pStrings) {
        cout << *s << " ";
    }
    cout << ")" << endl;

    return;
}

int main() {
    vector<string> inputStrings;
    string input;

    // user input: string values
    cout << "Enter your string values. Press <Enter> or <Space> after each "
            "input. Type '.' to stop."
         << endl;
    while (cin >> input && input != ".") {
        inputStrings.push_back(input);
    }
    cout << "---" << endl;

    if (inputStrings.size() == 0) {
        cerr << "ERROR: No string values entered. Restart to try again!"
             << endl;
        return -1;
    }

    // get vector of pointers
    vector<string *> pInputStrings = getPointerVector(inputStrings);
    // sort vector of pointers by values
    sort(pInputStrings);

    cout << "Ascending order of entered string values:" << endl;
    printVector(pInputStrings);
    return 0;
}
