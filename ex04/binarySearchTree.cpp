#include <iostream>
#include <string>

using namespace std;

struct Node {
    string key;
    Node *leftChild = nullptr;
    Node *rightChild = nullptr;
};

// inserts a key into tree with the given root node
void insert(Node *&node, const string &key) {
    // insert key into node, if node has empty key
    if (node->key == "") {
        node->key = key;
        return;
    }

    // abort inserting, if key already exists
    if (node->key == key) {
        return;
    }

    // insert key into left or right child node
    if (key < node->key) {
        if (node->leftChild) {
            insert(node->leftChild, key);
        } else {
            Node *child = new Node;
            node->leftChild = child;
            insert(node->leftChild, key);
        }
    } else {
        if (node->rightChild) {
            insert(node->rightChild, key);
        } else {
            Node *child = new Node;
            node->rightChild = child;
            insert(node->rightChild, key);
        }
    }

    return;
}

// traverses tree inorder and prints elements
void list(Node *&node) {
    // tree must be empty, if an empty key exists
    if (node->key == "") {
        cout << "No string values entered. Restart to try again!" << endl;
        return;
    }

    if (node->leftChild) {
        list(node->leftChild);
    }
    cout << node->key << " ";
    if (node->rightChild) {
        list(node->rightChild);
    }

    return;
}

// traverses tree postorder and deletes elements
void free(Node *&node) {
    if (node->leftChild) {
        free(node->leftChild);
    }
    if (node->rightChild) {
        free(node->rightChild);
    }

    delete node;

    return;
}

int main() {
    Node *root = new Node;
    string input;

    // user input: string values
    cout << "Enter your string values. Press <Enter> or <Space> after each "
            "input. Type '.' to stop."
         << endl;
    while (cin >> input && input != ".") {
        insert(root, input);
    }
    cout << "---" << endl;

    // print list
    cout << "Ascending order of entered string values:" << endl;
    list(root);

    // free space
    free(root);
    // change root pointer to prevent access
    root = nullptr;

    return 0;
}
