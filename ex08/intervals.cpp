#include <cassert>
#include <iostream>

using namespace std;

template <typename T> class Interval {
  private:
    T lowerBound;
    T upperBound;
    bool isValidInterval = true;

  public:
    Interval() : isValidInterval(false){};
    /** Creates an interval with the same upper and lower bound.
     *
     * @param a value for upper and lower bound
     */
    Interval(T a) : lowerBound(a), upperBound(a){};
    Interval(T a, T b) : lowerBound(min(a, b)), upperBound(max(a, b)){};

    void setInterval(const T &a, const T &b) {
        lowerBound = min(a, b);
        upperBound = max(a, b);
        isValidInterval = true;
    };

    T getLowerBound() {
        if (isValidInterval) {
            return lowerBound;
        }

        cerr << "Invalid Interval. Use Interval::setInterval() to create a "
                "valid interval. Returning 0."
             << endl;
        return 0;
    };

    T getUpperBound() {
        if (isValidInterval) {
            return upperBound;
        }

        cerr << "Invalid Interval. Use Interval::setInterval() to create a "
                "valid interval. Returning 0."
             << endl;
        return 0;
    };

    bool isValid() {
        return isValidInterval;
    };

    void print() {
        if (isValidInterval) {
            cout << "[" << lowerBound << ", " << upperBound << "]" << endl;
        } else {
            cerr << "Invalid Interval. Use Interval::setInterval() to "
                    "create a valid interval."
                 << endl;
        }
    };

    Interval operator+(const Interval &i) const {
        if (isValidInterval && i.isValidInterval) {
            return Interval<T>(lowerBound + i.lowerBound,
                               upperBound + i.upperBound);
        }

        return Interval<T>();
    };

    Interval operator-(const Interval &i) const {
        if (isValidInterval && i.isValidInterval) {
            return Interval<T>(lowerBound - i.lowerBound,
                               upperBound - i.upperBound);
        }

        return Interval<T>();
    };

    Interval operator*(const Interval &i) const {
        if (isValidInterval && i.isValidInterval) {
            return Interval<T>(lowerBound * i.lowerBound,
                               upperBound * i.upperBound);
        }

        return Interval<T>();
    };

    Interval operator/(const Interval &i) const {
        if (isValidInterval && i.isValidInterval && i.lowerBound != 0 &&
            i.upperBound != 0) {
            return Interval<T>(lowerBound / i.lowerBound,
                               upperBound / i.upperBound);
        }

        return Interval<T>();
    };

    Interval operator-() const {
        if (isValidInterval) {
            return Interval<T>(-lowerBound, -upperBound);
        }

        return Interval<T>();
    };
};

int main(int argc, char const *argv[]) {
    cout << "--- Constructors & Members ---" << endl;
    Interval<int> a;
    Interval<int> b(1);
    Interval<int> c(2, 1);

    assert(a.isValid() == false);
    assert(a.getLowerBound() == 0); // with error message
    assert(a.getUpperBound() == 0); // with error message

    assert(b.isValid() == true);
    assert(b.getLowerBound() == 1);
    assert(b.getUpperBound() == 1);

    assert(c.isValid() == true);
    assert(c.getLowerBound() == 1);
    assert(c.getUpperBound() == 2);

    cout << "a: ";
    a.print();
    cout << "b: ";
    b.print();
    cout << "c: ";
    c.print();

    cout << "--- Arithmetics & Types ---" << endl;
    Interval<double> d(1.5, 2.5);
    Interval<double> e(1.5, 0);

    assert(d.isValid() == true);
    assert(d.getLowerBound() == 1.5);
    assert(d.getUpperBound() == 2.5);

    assert(e.isValid() == true);
    assert(e.getLowerBound() == 0.0);
    assert(e.getUpperBound() == 1.5);

    cout << "d: ";
    d.print();
    cout << "e: ";
    e.print();

    Interval<double> f;
    f = d + e;
    cout << "f = d + e = ";
    f.print();
    assert(f.isValid() == true);
    assert(f.getLowerBound() == 1.5);
    assert(f.getUpperBound() == 4.0);

    f = d - e;
    cout << "f = d - e = ";
    f.print();
    assert(f.isValid() == true);
    assert(f.getLowerBound() == 1);
    assert(f.getUpperBound() == 1.5);

    f = d * e;
    cout << "f = d * e = ";
    f.print();
    assert(f.isValid() == true);
    assert(f.getLowerBound() == 0);
    assert(f.getUpperBound() == 3.75);

    f = d / e;
    cout << "f = d / e = ";
    f.print();
    assert(f.isValid() == false);
    e.setInterval(1, -2);
    cout << "e: ";
    e.print();
    f = d / e;
    cout << "f = d / e = ";
    f.print();
    assert(f.isValid() == true);
    assert(f.getLowerBound() == -0.75);
    assert(f.getUpperBound() == 2.5);

    f = -d;
    cout << "f = -d = ";
    f.print();
    assert(f.isValid() == true);
    assert(f.getLowerBound() == -2.5);
    assert(f.getUpperBound() == -1.5);

    f = -f;
    cout << "f = -f = ";
    f.print();
    assert(f.isValid() == true);
    assert(f.getLowerBound() == 1.5);
    assert(f.getUpperBound() == 2.5);

    cout << "--- Arithmetics with invalid Intervals ---" << endl;

    Interval<double> g;
    cout << "g: ";
    g.print();
    f = e + g;
    cout << "f = e + g = ";
    f.print();
    assert(f.isValid() == false);

    f = e - g;
    cout << "f = e - g = ";
    f.print();
    assert(f.isValid() == false);

    f = e * g;
    cout << "f = e * g = ";
    f.print();
    assert(f.isValid() == false);

    f = e / g;
    cout << "f = e / g = ";
    f.print();
    assert(f.isValid() == false);

    f = -g;
    cout << "f = -e = ";
    f.print();
    assert(f.isValid() == false);

    return 0;
}
