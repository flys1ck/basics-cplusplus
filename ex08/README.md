# Aufgabenblatt 8 - Operatoren und Templates

* Aufgabe 1 (Intervall-Arithmetik) - `intervals.cpp`

## Anmerkungen

Die Korrektheit des Programms wurde mit Hilfe von Assertions geprüft.

* `set` aus der Aufgabenstellung heißt im Programm `setInterval`
* `get` kommt im Programm unter `setLowerBound` und `setUpperBound` vor
* Ausgabe wurde mit Member-Funktion `print` realisiert