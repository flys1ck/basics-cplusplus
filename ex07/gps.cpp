#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Position {
  protected:
    double longitude;
    double latitude;
    double height; // relative height ellipsoidal earth model

  public:
    Position() {
        longitude = 0;
        latitude = 0;
        height = 0;
    }

    Position(const Position &p) {
        longitude = p.longitude;
        latitude = p.latitude;
        height = p.height;
    }

    Position(double _longitude, double _lattitude, double _height) {
        longitude = _longitude;
        latitude = _lattitude;
        height = _height;
    }

    double getLongitude() {
        return longitude;
    }

    virtual double getLatitude() {
        return latitude;
    }

    double getHeight() {
        return height;
    }

    virtual void print() {
        cout << "Longitude: " << longitude << ", Latitude: " << latitude
             << ", Height: " << height << endl;
    }
};

class Waypoint : public Position {
  private:
    string name;

  public:
    Waypoint() : Position() {
        name = "Unnamed waypoint";
    }

    Waypoint(const Waypoint &p) {
        longitude = p.longitude;
        latitude = p.latitude;
        height = p.height;
        name = p.name;
    }

    Waypoint(double _longitude, double _lattitude, double _height,
             const string &_name)
        : Position(_longitude, _lattitude, _height), name(_name) {}

    string getName() {
        return name;
    }

    virtual void print() {
        cout << "Longitude: " << longitude << ", Latitude: " << latitude
             << ", Height: " << height << ", Location: " << name << endl;
    }
};

class Trackpoint : public Position {
  private:
    int timestamp; // seconds since 5th Jan 1980

  public:
    Trackpoint() : Position() {
        timestamp = -1;
    }

    Trackpoint(const Trackpoint &p) {
        longitude = p.longitude;
        latitude = p.latitude;
        height = p.height;
        timestamp = p.timestamp;
    }

    Trackpoint(double _longitude, double _lattitude, double _height,
               int _timestamp)
        : Position(_longitude, _lattitude, _height), timestamp(_timestamp) {}

    int getTimestamp() {
        return timestamp;
    }

    virtual void print() {
        cout << "Longitude: " << longitude << ", Latitude: " << latitude
             << ", Height: " << height << ", Time: " << timestamp << endl;
    }
};

class Track {
  private:
    vector<Trackpoint> tracklog;

  public:
    Track(const Track &t) {
        tracklog = t.tracklog;
    }

    vector<Trackpoint> getTracklog() {
        return tracklog;
    }

    void addTrackpoint(Trackpoint trackpoint) {
        tracklog.push_back(trackpoint);
    };

    double getAverageHeight() {
        double avgHeight = 0;
        for (Trackpoint tp : tracklog) {
            avgHeight += tp.getHeight();
        }

        return avgHeight / tracklog.size();
    }

    void read(string path) {
        double longitude;
        double latitude;
        double height;
        int timestamp;

        ifstream ifs(path);
        while (ifs >> latitude >> longitude >> height >> timestamp) {
            addTrackpoint(Trackpoint(longitude, latitude, height, timestamp));
        }
    };

    void write(string path) {
        ofstream ofs(path);

        if (ofs.is_open()) {
            for (Trackpoint tp : tracklog) {
                ofs << fixed << setprecision(7) << tp.getLatitude() << " "
                    << tp.getLongitude() << " " << setprecision(2)
                    << tp.getHeight() << " " << tp.getTimestamp() << "\n";
            }
        }
    };

    virtual void print() {
        for (Trackpoint tp : tracklog) {
            tp.print();
        }
    }
};

int main(int argc, char const *argv[]) {
    // Track + Trackpoint
    Track track;
    vector<Trackpoint> tracklog;

    track.read("ex07/track.txt");
    track.addTrackpoint(Trackpoint(9.0, 56.0, 12.34, 61234));
    track.write("ex07/track2.txt");
    // track.print();

    tracklog = track.getTracklog();
    cout << "First Trackpoint: ";
    tracklog[0].print();
    cout << "Average height:" << track.getAverageHeight() << endl;
    cout << "---------" << endl;

    // Waypoint
    Waypoint wp1;
    wp1.print();
    Waypoint wp2(9.0, 56.0, 12.34, "Somewhere");
    wp2.print();
    cout << "---------" << endl;

    // Position
    Position pos1;
    pos1.print();
    Position pos2(9.0, 56.0, 12.34);
    pos2.print();

    return 0;
}
