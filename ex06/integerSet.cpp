#include <iostream>
#include <vector>

using namespace std;

class IntMenge {
  private:
    int minimum;
    int maximum;
    vector<bool> boolv;

  public:
    IntMenge(int min, int max) {
        minimum = min;
        maximum = max;
        if (min <= max) {
            // assign vector, +1 to also include maximum
            boolv.assign(max - min + 1, false);
        }
    };

    bool isValid(int i) {
        return i >= minimum && i <= maximum;
    };

    void add(int i) {
        if (isValid(i)) {
            boolv[i - minimum] = true;
        }
    };

    void remove(int i) {
        if (isValid(i)) {
            boolv[i - minimum] = false;
        }
    };

    bool contains(int i) {
        if (isValid(i)) {
            return boolv[i - minimum];
        }

        return false;
    };
    bool isEmpty() {
        if (getSize() == 0) {
            return true;
        }

        return false;
    };

    int getSize() {
        int size = 0;
        for (bool b : boolv) {
            if (b) {
                ++size;
            }
        }

        return size;
    };

    vector<int> getElements() {
        vector<int> intv;
        for (int i = 0; i < boolv.size(); ++i) {
            if (boolv[i]) {
                intv.push_back(i + minimum);
            }
        }

        return intv;
    };

    void print() {
        for (int i = 0; i < boolv.size(); ++i) {
            if (boolv[i]) {
                cout << i + minimum << " ";
            }
        }
        cout << "\n";
    };
};

int main(int argc, char const *argv[]) {
    IntMenge m(10, 100);
    int input;

    cin >> input;

    while (input != 0) {
        if (m.isValid(input))
            m.add(input);
        cout << "Menge enthaelt " << m.getSize() << " Element(e) (" << input
             << (m.contains(input) ? " enthalten)" : " nicht enthalten)")
             << endl;
        m.print();
        cin >> input;
    }

    vector<int> intv = m.getElements();

    for (int i = 0; i < intv.size(); i++)
        m.remove(intv[i]);

    cout << (m.isEmpty() ? "Menge ist leer" : "Menge ist nicht leer") << endl;

    return 0;
}
