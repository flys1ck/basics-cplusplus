#include <iostream>

using namespace std;

class Time {
  private:
    int minutes;

  public:
    Time() {
        cout << "Default constructor called!" << endl;
        minutes = 0;
    };

    Time(int m) {
        cout << "Minute constructor called!" << endl;
        if (m > 0) {
            minutes = m;
        }
    };

    Time(int h, int m) {
        cout << "Hour-Minute constructor called!" << endl;
        if (h > 0 && m > 0) {
            minutes = h * 60 + m;
        }
    };

    Time(const Time &t) {
        cout << "Copy constructor called!" << endl;
        minutes = t.minutes;
    };

    virtual void set(int h, int m) {
        minutes = h * 60 + m;
    };

    virtual void get(int &h, int &m) const {
        h = minutes / 60;
        m = minutes % 60;
    };

    virtual void inc() {
        minutes++;
    };

    virtual ~Time() {
        cout << "Destructor called!" << endl;
    }
};

void print(Time t) {
    int h, m;

    t.get(h, m);
    cout << h << ":" << m << endl;
}

Time input() {
    Time result;
    int h, m;

    cout << "Uhrzeit eingeben - Stunde: ";
    cin >> h;
    cout << "                   Minute: ";
    cin >> m;

    result.set(h, m);

    return result;
}

int main(int argc, char const *argv[]) {
    Time t1(10, 10);
    Time t2;

    t2 = input();
    t2.inc();

    print(t2);
    print(1234); // 20h 34min

    return 0;
}
