# Aufgabenblatt 6 - Klassen

* Aufgabe 1 (Konstruktoren) - `constructors.cpp`
* Aufgabe 2 (Integer-Menge) - `integerSet.cpp`

## Aufgabe 1: Erläuterung der Konstruktor- und Destruktoraufrufe:

Relevante Konsolenausgaben sind kommentiert.

```bash
Hour-Minute constructor called! # Konstruktor für t1 (Zeile 74)
Default constructor called! # Konstruktor für t2 (Zeile 75)
Default constructor called! # Konstruktor für result (Zeile 60)
Uhrzeit eingeben - Stunde: 50
                   Minute: 40
Destructor called! # Destruktor für result (Zeile 71)
Copy constructor called! # Konstuktor für Kopie von t2 auf t (Zeile 52)
50:41
Destructor called! # Destruktor für t (Zeile 57)
Minute constructor called! # Implizite Anwendung des Typumwandlungskonstruktors für 1234 auf t (Zeile 52)
20:34
Destructor called! # Destruktor für t (Zeile 57)
Destructor called! # Destruktor für t1 oder t2 (Zeile 84)
Destructor called! # Destruktor für t1 oder t2 (Zeile 84)
```