#include <cmath>
#include <iostream>

using namespace std;

int greatestCommonDivisor(int a, int b) {
    if (b == 0)
        return a;
    return greatestCommonDivisor(b, a % b);
}

void reduce(int numerator, int denominator) {
    int gcd = greatestCommonDivisor(abs(numerator), abs(denominator));
    // calc reduced numerator and denominator
    int reduced_numerator = numerator / gcd;
    int reduced_denominator = denominator / gcd;

    // switch signs
    if (reduced_denominator < 0) {
        reduced_numerator *= -1;
        reduced_denominator *= -1;
    }

    cout << reduced_numerator << "/" << reduced_denominator << endl;
    return;
}

int main() {
    int numerator, denominator;

    while (true) {
        cout << "Numerator: ";
        cin >> numerator;
        cout << "Denominator: ";
        cin >> denominator;
        reduce(numerator, denominator);
    }

    return 0;
}