#include <cmath>
#include <iostream>
#include <string>

using namespace std;

/**
 * Bot asks questions to determine a number in a given number range.
 *
 * For each iteration the range is divided into half and the user is asked if
 * his number is in the upper or the lower half. The function calls itself
 * recursively with the new range as input. The bot makes concrete guesses, if
 * the range is smaller than 2.
 *
 * @param min lower bound (inclusive)
 * @param max upper bound (inclusive)
 */
void makeGuesses(int min, int max) {
    int absolute = abs(max - min);
    string answer;

    if (absolute == 0) {
        cout << "Your number is " << min << "!" << endl;
    } else if (absolute == 1) {
        cout << "Is your number " << min << "? (y - yes, n - no)";
        getline(cin, answer);
        if (answer == "y") {
            cout << "Your number is " << min << "!" << endl;
        } else if (answer == "n") {
            cout << "Your number is " << max << "!" << endl;
        } else {
            cerr << "Your input is not valid. Try again!" << endl;
            makeGuesses(min, max);
        }
    } else {
        int median = min + ceil(absolute / 2.0);
        cout << "Is your number smaller than " << median
             << "? (y - yes, n - no) ";
        getline(cin, answer);
        if (answer == "y") {
            makeGuesses(min, median - 1);
        } else if (answer == "n") {
            makeGuesses(median, max);
        } else {
            cerr << "Your input is not valid. Try again!" << endl;
            makeGuesses(min, max);
        }
    }

    return;
}

int main() {
    int minimum, maximum;

    cout << "Please enter the lower boundary of the number range: ";
    cin >> minimum;
    cout << "Please enter the upper boundary of the number range: ";
    cin >> maximum;

    if (maximum < minimum) {
        cerr << "Your lower bound is greater than you upper bound. Restart to "
                "try again!"
             << endl;
        return -1;
    }

    cin.clear();
    cin.sync();

    makeGuesses(minimum, maximum);

    return 0;
}