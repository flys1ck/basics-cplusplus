#include <cmath>
#include <iostream>

using namespace std;

double calcPi(double precision) {
    double pi = 0;
    double pi_old = 0;
    int n = 0;
    double factor = 0;
    double difference; // difference between old and current iteration of pi

    do {
        // calculate factor with sign
        if (n % 2) {
            factor -= 1.0 / (2 * n + 1);
        } else {
            factor += 1.0 / (2 * n + 1);
        }
        // calculate current iteration of pi, save old pi
        pi_old = pi;
        pi = 4 * factor;
        // calculate difference
        difference = abs(pi - pi_old);
        n++;
    } while (difference >= precision);

    return pi;
}

int main() {
    double precision;
    do {
        cout << "Please enter a desired precision (greater 0) for pi: ";
        cin >> precision;
    } while (precision <= 0);

    double pi = calcPi(precision);

    cout << "Pi is " << pi << " with an error of " << abs(pi - M_PI) << endl;
}